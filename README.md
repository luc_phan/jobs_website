### Development ###

Vagrant setup :

```
$ cat ~/.ssh/id_rsa.pub
$ echo "Add SSH key to Bitbucket"
$ mkdir ~/work
$ cd ~/work
$ git clone 'git@bitbucket.org:luc_phan/quoonel-httpclient-vagrant.git'
$ cd quoonel-httpclient-vagrant
$ vagrant up
$ vagrant ssh
```

Cloning :

```
$ ssh-keygen
$ cat ~/.ssh/id_rsa.pub
$ echo "Add SSH key to Bitbucket"
$ mkdir ~/work
$ cd ~/work
$ git clone 'git@bitbucket.org:luc_phan/jobs_website.git'
```

Dependencies :

```
$ pip install django
$ yum install npm
$ npm install -g bower
$ pip install django-bower
```

Static files :

```
$ cd ~/work/jobs_website
$ python manage.py bower install
$ python manage.py collectstatic
```
Database initialization :

```
$ cd ~/work/jobs_website
$ python manage.py migrate
```

Firewall :

```
$ sudo firewall-cmd --zone=public --add-port=8000/tcp --permanent
$ sudo firewall-cmd --reload
$ sudo firewall-cmd --list-ports
```

Testing :

```
$ cd ~/work/jobs_website
$ python manage.py runserver 0.0.0.0:8000
```